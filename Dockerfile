FROM fredblgr/ubuntu-novnc:20.04

# Expose the necessary port
EXPOSE 80

# Start the novnc server
CMD ["supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
